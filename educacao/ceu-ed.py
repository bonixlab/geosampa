# -*- coding: utf-8 -*-

import re
    
#def filterFeature(ogrfeature, fieldNames, reproject):
#    if not ogrfeature:
#        return
#
#    if ogrfeature.GetFieldAsString('cd_distr') == "45":
#        return ogrfeature
#    else:
#        return
    
def filterTags(attrs):
    if not attrs:
        return
    tags = {}
    
    if 1 > 0:
        ##tags['height'] = str(float(attrs['ALTURA_EDI'])).replace(".",",")
        # tags['height'] = str(float(attrs['ALTURA_EDI']))
        #tags['height'] = str(float(attrs['altura']))
        #tags['height'] = str(float(attrs['ed_altura']))

        #tags['name'] = (attrs['eq_nome']).title
        tags['addr:district'] = (attrs['eq_subpref']).title()
        tags['addr:subdistrict'] = (attrs['eq_bairro']).title()
        tags['addr:street'] = (attrs['eq_ender']).title()


        tags['addr:postal_code'] = ([attrs['eq_cep']][0][0:5] +'-'+ [attrs['eq_cep']][0][5:8])

        #tags['addr:postal_code'] = (attrs['eq_cep'])
        tags['email'] = (attrs['eq_email']).lower()
        tags['addr:housenumber'] = attrs['eq_num'] 

        tags['openning_hours'] = 'Mo-Fr ' + attrs['eq_horario']

        tags['fax'] = '+55 11 ' + str( [(attrs['eq_fax'])][0][0:4] ) +'-'+ str( [(attrs['eq_fax'])][0][4:8] ) 

        tags['phone'] = '+55 11 ' + str( [(attrs['eq_fone1'])][0][0:4] ) +'-'+ str( [(attrs['eq_fone1'])][0][4:8] ) + ';' + '+55 11 ' + str( [(attrs['eq_fone2'])][0][0:4] ) +'-'+ str( [(attrs['eq_fone2'])][0][4:8] )
        #tags['phone'] = attrs['eq_fone1']


    tags['source'] = 'pmsp'
    tags['operator'] = 'PMSP'

    return tags

